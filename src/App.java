import model.Person;

public class App {
    public static void main(String[] args) throws Exception {
        Person person1 = new Person();

        Person person2 = new Person("Devcamp", "User", "Male", "du", "du@devcamp.edu.vn");

        System.out.println(person1.toString());
        System.out.println(person2.toString());

        System.out.println(person1.getFirstName());
        System.out.println(person2.getFirstName());

        person1.setFirstName("Update");

        System.out.println(person1.getFirstName());
        System.out.println(person2.getFirstName());
    }
}
